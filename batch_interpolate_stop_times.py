import os
import pandas as pd
import arcpy as ap

ap.ImportToolbox(r"K:\Projects\Tools\InterpolateBlankStopTimes_0.1.1.0\InterpolateBlankStopTimes.tbx")

#top = ap.GetParameterAsText(0)


def gtfs_batch_directory(top):
    walk_file(top)


def walk_file(top):
    """
    Takes in directory containing GTFS files, creates a list of directories
    containing stop times files and passes them to the stop_times_check function
    and passes them to the stop_times_check function
    """
    gtfs_folders = []
    for root, dirs, files in os.walk(top):
        if "stop_times.txt" in files:
            gtfs_folders.append(root)
    stop_times_check(gtfs_folders)


def stop_times_check(gtfs_folders):
    """
    Creates dataframe of stop_times.txt file and checks for data in arrival
    and departure columns. If there are empty values, times are interpolated.
    Output is placed in a new directory within each gtfs folder.
    """
    for gtfs in gtfs_folders:
        stop_times = pd.read_csv(os.path.join(gtfs,"stop_times.txt"))
        missing_values_bool = (stop_times['arrival_time'].isnull()) | (stop_times['departure_time'].isnull())
        if stop_times[missing_values_bool].empty:
            print(os.path.basename + " has stop times")
        else:
            agency_name = os.path.basename(gtfs)
            outfolder = os.path.join(gtfs, agency_name + "interpolate_output")
            current_stop_times = os.path.join(gtfs, "stop_times.txt")
            new_stop_times = os.path.join(gtfs, outfolder, agency_name + "_stop_times.txt")
            sql_database = os.path.join(gtfs, outfolder, agency_name + ".sql")
            os.mkdir(outfolder)
            ap.PreprocessStopTimes_transit(current_stop_times, sql_database)
            ap.SimpleInterpolation_transit(sql_database, new_stop_times)